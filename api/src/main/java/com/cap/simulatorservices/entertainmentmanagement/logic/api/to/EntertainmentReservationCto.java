package com.cap.simulatorservices.entertainmentmanagement.logic.api.to;

import com.devonfw.module.basic.common.api.to.AbstractCto;

/**
 * Composite transport object of EntertainmentReservation
 */
public class EntertainmentReservationCto extends AbstractCto {

  private static final long serialVersionUID = 1L;

  private EntertainmentReservationEto entertainmentReservation;

  private CustomerEto customerId;

  private ShowDetailsEto showId;

  public EntertainmentReservationEto getEntertainmentReservation() {

    return entertainmentReservation;
  }

  public void setEntertainmentReservation(EntertainmentReservationEto entertainmentReservation) {

    this.entertainmentReservation = entertainmentReservation;
  }

  public CustomerEto getCustomerId() {

    return customerId;
  }

  public void setCustomerId(CustomerEto customerId) {

    this.customerId = customerId;
  }

  public ShowDetailsEto getShowId() {

    return showId;
  }

  public void setShowId(ShowDetailsEto showId) {

    this.showId = showId;
  }

}
