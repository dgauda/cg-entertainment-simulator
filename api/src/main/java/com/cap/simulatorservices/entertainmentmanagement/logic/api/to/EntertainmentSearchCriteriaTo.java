package com.cap.simulatorservices.entertainmentmanagement.logic.api.to;

import com.cap.simulatorservices.general.common.api.to.AbstractSearchCriteriaTo;
import com.devonfw.module.basic.common.api.query.StringSearchConfigTo;

/**
 * {@link SearchCriteriaTo} to find instances of
 * {@link com.cap.simulatorservices.entertainmentmanagement.common.api.Entertainment}s.
 */
public class EntertainmentSearchCriteriaTo extends AbstractSearchCriteriaTo {

  private static final long serialVersionUID = 1L;

  private String address;

  private Integer zip;

  private String entertainmentType;

  private StringSearchConfigTo addressOption;

  private StringSearchConfigTo entertainmentTypeOption;

  /**
   * @return addressId
   */

  public String getAddress() {

    return address;
  }

  /**
   * @param address setter for address attribute
   */

  public void setAddress(String address) {

    this.address = address;
  }

  /**
   * @return zipId
   */

  public Integer getZip() {

    return zip;
  }

  /**
   * @param zip setter for zip attribute
   */

  public void setZip(Integer zip) {

    this.zip = zip;
  }

  /**
   * @return entertainmentTypeId
   */

  public String getEntertainmentType() {

    return entertainmentType;
  }

  /**
   * @param entertainmentType setter for entertainmentType attribute
   */

  public void setEntertainmentType(String entertainmentType) {

    this.entertainmentType = entertainmentType;
  }

  /**
   * @return the {@link StringSearchConfigTo} used to search for {@link #getAddress() address}.
   */
  public StringSearchConfigTo getAddressOption() {

    return this.addressOption;
  }

  /**
   * @param addressOption new value of {@link #getAddressOption()}.
   */
  public void setAddressOption(StringSearchConfigTo addressOption) {

    this.addressOption = addressOption;
  }

  /**
   * @return the {@link StringSearchConfigTo} used to search for {@link #getEntertainmentType() entertainmentType}.
   */
  public StringSearchConfigTo getEntertainmentTypeOption() {

    return this.entertainmentTypeOption;
  }

  /**
   * @param entertainmentTypeOption new value of {@link #getEntertainmentTypeOption()}.
   */
  public void setEntertainmentTypeOption(StringSearchConfigTo entertainmentTypeOption) {

    this.entertainmentTypeOption = entertainmentTypeOption;
  }

}
