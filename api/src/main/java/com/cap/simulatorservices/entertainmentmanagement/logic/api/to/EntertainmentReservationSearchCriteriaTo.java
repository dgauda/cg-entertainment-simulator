package com.cap.simulatorservices.entertainmentmanagement.logic.api.to;

import java.sql.Timestamp;

import com.cap.simulatorservices.general.common.api.to.AbstractSearchCriteriaTo;

/**
 * {@link SearchCriteriaTo} to find instances of
 * {@link com.cap.simulatorservices.entertainmentmanagement.common.api.EntertainmentReservation}s.
 */
public class EntertainmentReservationSearchCriteriaTo extends AbstractSearchCriteriaTo {

  private static final long serialVersionUID = 1L;

  private Timestamp bookingDate;

  private Integer noOfTickets;

  private Long customerIdId;

  private Long showIdId;

  /**
   * @return bookingDateId
   */

  public Timestamp getBookingDate() {

    return this.bookingDate;
  }

  /**
   * @param bookingDate setter for bookingDate attribute
   */

  public void setBookingDate(Timestamp bookingDate) {

    this.bookingDate = bookingDate;
  }

  /**
   * @return noOfTicketsId
   */

  public Integer getNoOfTickets() {

    return this.noOfTickets;
  }

  /**
   * @param noOfTickets setter for noOfTickets attribute
   */

  public void setNoOfTickets(Integer noOfTickets) {

    this.noOfTickets = noOfTickets;
  }

  /**
   * getter for customerIdId attribute
   *
   * @return customerIdId
   */

  public Long getCustomerIdId() {

    return this.customerIdId;
  }

  /**
   * @param customerId setter for customerId attribute
   */

  public void setCustomerIdId(Long customerIdId) {

    this.customerIdId = customerIdId;
  }

  /**
   * getter for showIdId attribute
   *
   * @return showIdId
   */

  public Long getShowIdId() {

    return this.showIdId;
  }

  /**
   * @param showId setter for showId attribute
   */

  public void setShowIdId(Long showIdId) {

    this.showIdId = showIdId;
  }

}
