package com.cap.simulatorservices.entertainmentmanagement.logic.api.to;

import java.sql.Timestamp;

import com.cap.simulatorservices.general.common.api.to.AbstractSearchCriteriaTo;
import com.devonfw.module.basic.common.api.query.StringSearchConfigTo;

/**
 * {@link SearchCriteriaTo} to find instances of
 * {@link com.cap.simulatorservices.entertainmentmanagement.common.api.ShowDetails}s.
 */
public class ShowDetailsSearchCriteriaTo extends AbstractSearchCriteriaTo {

  private static final long serialVersionUID = 1L;

  private Timestamp showTime;

  private String classType;

  private Long entertainmentIdId;

  private StringSearchConfigTo classTypeOption;

  /**
   * @return showTimeId
   */

  public Timestamp getShowTime() {

    return this.showTime;
  }

  /**
   * @param showTime setter for showTime attribute
   */

  public void setShowTime(Timestamp showTime) {

    this.showTime = showTime;
  }

  /**
   * @return classTypeId
   */

  public String getClassType() {

    return this.classType;
  }

  /**
   * @param classType setter for classType attribute
   */

  public void setClassType(String classType) {

    this.classType = classType;
  }

  /**
   * getter for entertainmentIdId attribute
   *
   * @return entertainmentIdId
   */

  public Long getEntertainmentIdId() {

    return this.entertainmentIdId;
  }

  /**
   * @param entertainmentId setter for entertainmentId attribute
   */

  public void setEntertainmentIdId(Long entertainmentIdId) {

    this.entertainmentIdId = entertainmentIdId;
  }

  /**
   * @return the {@link StringSearchConfigTo} used to search for {@link #getClassType() classType}.
   */
  public StringSearchConfigTo getClassTypeOption() {

    return this.classTypeOption;
  }

  /**
   * @param classTypeOption new value of {@link #getClassTypeOption()}.
   */
  public void setClassTypeOption(StringSearchConfigTo classTypeOption) {

    this.classTypeOption = classTypeOption;
  }

}
