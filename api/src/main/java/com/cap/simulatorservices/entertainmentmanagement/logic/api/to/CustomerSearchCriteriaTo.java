package com.cap.simulatorservices.entertainmentmanagement.logic.api.to;

import java.sql.Timestamp;

import com.cap.simulatorservices.general.common.api.to.AbstractSearchCriteriaTo;
import com.devonfw.module.basic.common.api.query.StringSearchConfigTo;

/**
 * {@link SearchCriteriaTo} to find instances of
 * {@link com.cap.simulatorservices.entertainmentmanagement.common.api.Customer}s.
 */
public class CustomerSearchCriteriaTo extends AbstractSearchCriteriaTo {

  private static final long serialVersionUID = 1L;

  private String name;

  private Integer contactNumber;

  private Boolean isRegularCustomer;

  private Timestamp registrationDate;

  private StringSearchConfigTo nameOption;

  /**
   * @return nameId
   */

  public String getName() {

    return this.name;
  }

  /**
   * @param name setter for name attribute
   */

  public void setName(String name) {

    this.name = name;
  }

  /**
   * @return contactNumberId
   */

  public Integer getContactNumber() {

    return this.contactNumber;
  }

  /**
   * @param contactNumber setter for contactNumber attribute
   */

  public void setContactNumber(Integer contactNumber) {

    this.contactNumber = contactNumber;
  }

  /**
   * @return isRegularCustomerId
   */

  public Boolean getIsRegularCustomer() {

    return this.isRegularCustomer;
  }

  /**
   * @param isRegularCustomer setter for isRegularCustomer attribute
   */

  public void setIsRegularCustomer(Boolean isRegularCustomer) {

    this.isRegularCustomer = isRegularCustomer;
  }

  /**
   * @return registrationDateId
   */

  public Timestamp getRegistrationDate() {

    return this.registrationDate;
  }

  /**
   * @param registrationDate setter for registrationDate attribute
   */

  public void setRegistrationDate(Timestamp registrationDate) {

    this.registrationDate = registrationDate;
  }

  /**
   * @return the {@link StringSearchConfigTo} used to search for {@link #getName() name}.
   */
  public StringSearchConfigTo getNameOption() {

    return this.nameOption;
  }

  /**
   * @param nameOption new value of {@link #getNameOption()}.
   */
  public void setNameOption(StringSearchConfigTo nameOption) {

    this.nameOption = nameOption;
  }

}
