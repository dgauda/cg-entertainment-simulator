package com.cap.simulatorservices.entertainmentmanagement.logic.api.to;

import java.sql.Timestamp;

import com.cap.simulatorservices.entertainmentmanagement.common.api.ShowDetails;
import com.devonfw.module.basic.common.api.to.AbstractEto;

/**
 * Entity transport object of ShowDetails
 */
public class ShowDetailsEto extends AbstractEto implements ShowDetails {

  private static final long serialVersionUID = 1L;

  private Timestamp showTime;

  private String classType;

  private Long entertainmentId;

  @Override
  public Timestamp getShowTime() {

    return this.showTime;
  }

  @Override
  public void setShowTime(Timestamp showTime) {

    this.showTime = showTime;
  }

  @Override
  public String getClassType() {

    return this.classType;
  }

  @Override
  public void setClassType(String classType) {

    this.classType = classType;
  }

  @Override
  public Long getEntertainmentId() {

    return this.entertainmentId;
  }

  @Override
  public void setEntertainmentId(Long entertainmentId) {

    this.entertainmentId = entertainmentId;
  }

  @Override
  public int hashCode() {

    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + ((this.showTime == null) ? 0 : this.showTime.hashCode());
    result = prime * result + ((this.classType == null) ? 0 : this.classType.hashCode());

    result = prime * result + ((this.entertainmentId == null) ? 0 : this.entertainmentId.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {

    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    // class check will be done by super type EntityTo!
    if (!super.equals(obj)) {
      return false;
    }
    ShowDetailsEto other = (ShowDetailsEto) obj;
    if (this.showTime == null) {
      if (other.showTime != null) {
        return false;
      }
    } else if (!this.showTime.equals(other.showTime)) {
      return false;
    }
    if (this.classType == null) {
      if (other.classType != null) {
        return false;
      }
    } else if (!this.classType.equals(other.classType)) {
      return false;
    }

    if (this.entertainmentId == null) {
      if (other.entertainmentId != null) {
        return false;
      }
    } else if (!this.entertainmentId.equals(other.entertainmentId)) {
      return false;
    }
    return true;
  }
}
