package com.cap.simulatorservices.entertainmentmanagement.logic.api.to;

import java.sql.Timestamp;

import com.cap.simulatorservices.entertainmentmanagement.common.api.EntertainmentReservation;
import com.devonfw.module.basic.common.api.to.AbstractEto;

/**
 * Entity transport object of EntertainmentReservation
 */
public class EntertainmentReservationEto extends AbstractEto implements EntertainmentReservation {

  private static final long serialVersionUID = 1L;

  private Timestamp bookingDate;

  private Integer noOfTickets;

  private Long customerId;

  private Long showId;

  @Override
  public Timestamp getBookingDate() {

    return this.bookingDate;
  }

  @Override
  public void setBookingDate(Timestamp bookingDate) {

    this.bookingDate = bookingDate;
  }

  @Override
  public Integer getNoOfTickets() {

    return this.noOfTickets;
  }

  @Override
  public void setNoOfTickets(Integer noOfTickets) {

    this.noOfTickets = noOfTickets;
  }

  @Override
  public Long getCustomerId() {

    return this.customerId;
  }

  @Override
  public void setCustomerId(Long customerId) {

    this.customerId = customerId;
  }

  @Override
  public Long getShowId() {

    return this.showId;
  }

  @Override
  public void setShowId(Long showId) {

    this.showId = showId;
  }

  @Override
  public int hashCode() {

    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + ((this.bookingDate == null) ? 0 : this.bookingDate.hashCode());
    result = prime * result + ((this.noOfTickets == null) ? 0 : this.noOfTickets.hashCode());

    result = prime * result + ((this.customerId == null) ? 0 : this.customerId.hashCode());

    result = prime * result + ((this.showId == null) ? 0 : this.showId.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {

    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    // class check will be done by super type EntityTo!
    if (!super.equals(obj)) {
      return false;
    }
    EntertainmentReservationEto other = (EntertainmentReservationEto) obj;
    if (this.bookingDate == null) {
      if (other.bookingDate != null) {
        return false;
      }
    } else if (!this.bookingDate.equals(other.bookingDate)) {
      return false;
    }
    if (this.noOfTickets == null) {
      if (other.noOfTickets != null) {
        return false;
      }
    } else if (!this.noOfTickets.equals(other.noOfTickets)) {
      return false;
    }

    if (this.customerId == null) {
      if (other.customerId != null) {
        return false;
      }
    } else if (!this.customerId.equals(other.customerId)) {
      return false;
    }

    if (this.showId == null) {
      if (other.showId != null) {
        return false;
      }
    } else if (!this.showId.equals(other.showId)) {
      return false;
    }
    return true;
  }
}
