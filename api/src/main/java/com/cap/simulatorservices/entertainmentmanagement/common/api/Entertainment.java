package com.cap.simulatorservices.entertainmentmanagement.common.api;

import com.cap.simulatorservices.general.common.api.ApplicationEntity;

public interface Entertainment extends ApplicationEntity {

  /**
   * @return addressId
   */

  public String getAddress();

  /**
   * @param address setter for address attribute
   */

  public void setAddress(String address);

  /**
   * @return zipId
   */

  public Integer getZip();

  /**
   * @param zip setter for zip attribute
   */

  public void setZip(Integer zip);

  /**
   * @return entertainmentTypeId
   */

  public String getEntertainmentType();

  /**
   * @param entertainmentType setter for entertainmentType attribute
   */

  public void setEntertainmentType(String entertainmentType);

}
