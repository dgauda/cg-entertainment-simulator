package com.cap.simulatorservices.entertainmentmanagement.common.api;

import java.sql.Timestamp;

import com.cap.simulatorservices.general.common.api.ApplicationEntity;

public interface Customer extends ApplicationEntity {

  /**
   * @return nameId
   */

  public String getName();

  /**
   * @param name setter for name attribute
   */

  public void setName(String name);

  /**
   * @return contactNumberId
   */

  public Integer getContactNumber();

  /**
   * @param contactNumber setter for contactNumber attribute
   */

  public void setContactNumber(Integer contactNumber);

  /**
   * @return isRegularCustomerId
   */

  public Boolean getIsRegularCustomer();

  /**
   * @param isRegularCustomer setter for isRegularCustomer attribute
   */

  public void setIsRegularCustomer(Boolean isRegularCustomer);

  /**
   * @return registrationDateId
   */

  public Timestamp getRegistrationDate();

  /**
   * @param registrationDate setter for registrationDate attribute
   */

  public void setRegistrationDate(Timestamp registrationDate);

}
