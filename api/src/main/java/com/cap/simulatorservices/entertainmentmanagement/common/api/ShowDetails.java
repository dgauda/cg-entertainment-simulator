package com.cap.simulatorservices.entertainmentmanagement.common.api;

import java.sql.Timestamp;

import com.cap.simulatorservices.general.common.api.ApplicationEntity;

public interface ShowDetails extends ApplicationEntity {

  /**
   * @return showTimeId
   */

  public Timestamp getShowTime();

  /**
   * @param showTime setter for showTime attribute
   */

  public void setShowTime(Timestamp showTime);

  /**
   * @return classTypeId
   */

  public String getClassType();

  /**
   * @param classType setter for classType attribute
   */

  public void setClassType(String classType);

  /**
   * getter for entertainmentIdId attribute
   *
   * @return entertainmentIdId
   */

  public Long getEntertainmentId();

  /**
   * @param entertainmentId setter for entertainmentId attribute
   */

  public void setEntertainmentId(Long entertainmentId);

}
