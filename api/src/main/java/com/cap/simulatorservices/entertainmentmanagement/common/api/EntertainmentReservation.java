package com.cap.simulatorservices.entertainmentmanagement.common.api;

import java.sql.Timestamp;

import com.cap.simulatorservices.general.common.api.ApplicationEntity;

public interface EntertainmentReservation extends ApplicationEntity {

  /**
   * @return bookingDateId
   */

  public Timestamp getBookingDate();

  /**
   * @param bookingDate setter for bookingDate attribute
   */

  public void setBookingDate(Timestamp bookingDate);

  /**
   * @return noOfTicketsId
   */

  public Integer getNoOfTickets();

  /**
   * @param noOfTickets setter for noOfTickets attribute
   */

  public void setNoOfTickets(Integer noOfTickets);

  /**
   * getter for customerIdId attribute
   *
   * @return customerIdId
   */

  public Long getCustomerId();

  /**
   * @param customerId setter for customerId attribute
   */

  public void setCustomerId(Long customerId);

  /**
   * getter for showIdId attribute
   *
   * @return showIdId
   */

  public Long getShowId();

  /**
   * @param showId setter for showId attribute
   */

  public void setShowId(Long showId);

}
