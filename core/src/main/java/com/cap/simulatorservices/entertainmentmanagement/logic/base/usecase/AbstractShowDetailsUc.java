package com.cap.simulatorservices.entertainmentmanagement.logic.base.usecase;

import javax.inject.Inject;

import com.cap.simulatorservices.entertainmentmanagement.dataaccess.api.repo.ShowDetailsRepository;
import com.cap.simulatorservices.general.logic.base.AbstractUc;

/**
 * Abstract use case for ShowDetailss, which provides access to the commonly necessary data access objects.
 */
public class AbstractShowDetailsUc extends AbstractUc {

  /** @see #getShowDetailsRepository() */
  @Inject
  private ShowDetailsRepository showDetailsRepository;

  /**
   * Returns the field 'showDetailsRepository'.
   * 
   * @return the {@link ShowDetailsRepository} instance.
   */
  public ShowDetailsRepository getShowDetailsRepository() {

    return this.showDetailsRepository;
  }

}
