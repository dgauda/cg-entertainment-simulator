package com.cap.simulatorservices.entertainmentmanagement.logic.impl;

import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.data.domain.Page;

import com.cap.simulatorservices.entertainmentmanagement.logic.api.Entertainmentmanagement;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.CustomerCto;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.CustomerSearchCriteriaTo;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.EntertainmentCto;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.EntertainmentReservationCto;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.EntertainmentReservationSearchCriteriaTo;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.EntertainmentSearchCriteriaTo;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.ShowDetailsCto;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.ShowDetailsSearchCriteriaTo;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.usecase.UcFindCustomer;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.usecase.UcFindEntertainment;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.usecase.UcFindEntertainmentReservation;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.usecase.UcFindShowDetails;
import com.cap.simulatorservices.general.logic.base.AbstractComponentFacade;

/**
 * Implementation of component interface of entertainmentmanagement
 */
@Named
public class EntertainmentmanagementImpl extends AbstractComponentFacade implements Entertainmentmanagement {

  @Inject
  private UcFindCustomer ucFindCustomer;

  @Inject
  private UcFindEntertainment ucFindEntertainment;

  @Inject
  private UcFindEntertainmentReservation ucFindEntertainmentReservation;

  @Inject
  private UcFindShowDetails ucFindShowDetails;

  @Override
  public CustomerCto findCustomerCto(long id) {

    return ucFindCustomer.findCustomerCto(id);
  }

  @Override
  public Page<CustomerCto> findCustomerCtos(CustomerSearchCriteriaTo criteria) {

    return ucFindCustomer.findCustomerCtos(criteria);
  }

  @Override
  public EntertainmentCto findEntertainmentCto(long id) {

    return ucFindEntertainment.findEntertainmentCto(id);
  }

  @Override
  public Page<EntertainmentCto> findEntertainmentCtos(EntertainmentSearchCriteriaTo criteria) {

    return ucFindEntertainment.findEntertainmentCtos(criteria);
  }

  @Override
  public EntertainmentReservationCto findEntertainmentReservationCto(long id) {

    return ucFindEntertainmentReservation.findEntertainmentReservationCto(id);
  }

  @Override
  public Page<EntertainmentReservationCto> findEntertainmentReservationCtos(
      EntertainmentReservationSearchCriteriaTo criteria) {

    return ucFindEntertainmentReservation.findEntertainmentReservationCtos(criteria);
  }

  @Override
  public ShowDetailsCto findShowDetailsCto(long id) {

    return ucFindShowDetails.findShowDetailsCto(id);
  }

  @Override
  public Page<ShowDetailsCto> findShowDetailsCtos(ShowDetailsSearchCriteriaTo criteria) {

    return ucFindShowDetails.findShowDetailsCtos(criteria);
  }

}
