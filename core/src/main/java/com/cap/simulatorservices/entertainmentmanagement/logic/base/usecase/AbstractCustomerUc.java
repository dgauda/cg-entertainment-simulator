package com.cap.simulatorservices.entertainmentmanagement.logic.base.usecase;

import javax.inject.Inject;

import com.cap.simulatorservices.entertainmentmanagement.dataaccess.api.repo.CustomerRepository;
import com.cap.simulatorservices.general.logic.base.AbstractUc;

/**
 * Abstract use case for Customers, which provides access to the commonly necessary data access objects.
 */
public class AbstractCustomerUc extends AbstractUc {

  /** @see #getCustomerRepository() */
  @Inject
  private CustomerRepository customerRepository;

  /**
   * Returns the field 'customerRepository'.
   * 
   * @return the {@link CustomerRepository} instance.
   */
  public CustomerRepository getCustomerRepository() {

    return this.customerRepository;
  }

}
