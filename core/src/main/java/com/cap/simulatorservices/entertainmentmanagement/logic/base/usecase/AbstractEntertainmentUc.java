package com.cap.simulatorservices.entertainmentmanagement.logic.base.usecase;

import javax.inject.Inject;

import com.cap.simulatorservices.entertainmentmanagement.dataaccess.api.repo.EntertainmentRepository;
import com.cap.simulatorservices.general.logic.base.AbstractUc;

/**
 * Abstract use case for Entertainments, which provides access to the commonly necessary data access objects.
 */
public class AbstractEntertainmentUc extends AbstractUc {

  /** @see #getEntertainmentRepository() */
  @Inject
  private EntertainmentRepository entertainmentRepository;

  /**
   * Returns the field 'entertainmentRepository'.
   * 
   * @return the {@link EntertainmentRepository} instance.
   */
  public EntertainmentRepository getEntertainmentRepository() {

    return this.entertainmentRepository;
  }

}
