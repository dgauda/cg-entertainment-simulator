package com.cap.simulatorservices.entertainmentmanagement.service.impl.rest;

import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.data.domain.Page;

import com.cap.simulatorservices.entertainmentmanagement.logic.api.Entertainmentmanagement;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.CustomerCto;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.CustomerSearchCriteriaTo;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.EntertainmentCto;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.EntertainmentReservationCto;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.EntertainmentReservationSearchCriteriaTo;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.EntertainmentSearchCriteriaTo;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.ShowDetailsCto;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.ShowDetailsSearchCriteriaTo;
import com.cap.simulatorservices.entertainmentmanagement.service.api.rest.EntertainmentmanagementRestService;

/**
 * The service implementation for REST calls in order to execute the logic of component {@link Entertainmentmanagement}.
 */
@Named("EntertainmentmanagementRestService")
public class EntertainmentmanagementRestServiceImpl implements EntertainmentmanagementRestService {

  @Inject
  private Entertainmentmanagement entertainmentmanagement;

  @Override
  public CustomerCto getCustomerCto(long id) {

    return this.entertainmentmanagement.findCustomerCto(id);
  }

  @Override
  public Page<CustomerCto> findCustomerCtos(CustomerSearchCriteriaTo searchCriteriaTo) {

    return this.entertainmentmanagement.findCustomerCtos(searchCriteriaTo);
  }

  @Override
  public EntertainmentCto getEntertainmentCto(long id) {

    return this.entertainmentmanagement.findEntertainmentCto(id);
  }

  @Override
  public Page<EntertainmentCto> findEntertainmentCtos(EntertainmentSearchCriteriaTo searchCriteriaTo) {

    return this.entertainmentmanagement.findEntertainmentCtos(searchCriteriaTo);
  }

  @Override
  public EntertainmentReservationCto getEntertainmentReservationCto(long id) {

    return this.entertainmentmanagement.findEntertainmentReservationCto(id);
  }

  @Override
  public Page<EntertainmentReservationCto> findEntertainmentReservationCtos(
      EntertainmentReservationSearchCriteriaTo searchCriteriaTo) {

    return this.entertainmentmanagement.findEntertainmentReservationCtos(searchCriteriaTo);
  }

  @Override
  public ShowDetailsCto getShowDetailsCto(long id) {

    return this.entertainmentmanagement.findShowDetailsCto(id);
  }

  @Override
  public Page<ShowDetailsCto> findShowDetailsCtos(ShowDetailsSearchCriteriaTo searchCriteriaTo) {

    return this.entertainmentmanagement.findShowDetailsCtos(searchCriteriaTo);
  }

}
