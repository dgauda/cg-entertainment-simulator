package com.cap.simulatorservices.entertainmentmanagement.logic.api.usecase;

import java.util.List;

import org.springframework.data.domain.Page;

import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.EntertainmentCto;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.EntertainmentSearchCriteriaTo;

public interface UcFindEntertainment {

  /**
   * Returns a composite Entertainment by its id 'id'.
   *
   * @param id The id 'id' of the Entertainment.
   * @return The {@link EntertainmentCto} with id 'id'
   */
  EntertainmentCto findEntertainmentCto(long id);

  /**
   * Returns a paginated list of composite Entertainments matching the search criteria.
   *
   * @param criteria the {@link EntertainmentSearchCriteriaTo}.
   * @return the {@link List} of matching {@link EntertainmentCto}s.
   */
  Page<EntertainmentCto> findEntertainmentCtos(EntertainmentSearchCriteriaTo criteria);

}
