package com.cap.simulatorservices.entertainmentmanagement.logic.impl.usecase;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.cap.simulatorservices.entertainmentmanagement.dataaccess.api.EntertainmentEntity;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.EntertainmentCto;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.EntertainmentEto;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.EntertainmentSearchCriteriaTo;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.usecase.UcFindEntertainment;
import com.cap.simulatorservices.entertainmentmanagement.logic.base.usecase.AbstractEntertainmentUc;

/**
 * Use case implementation for searching, filtering and getting Entertainments
 */
@Named
@Validated
@Transactional
public class UcFindEntertainmentImpl extends AbstractEntertainmentUc implements UcFindEntertainment {

  /** Logger instance. */
  private static final Logger LOG = LoggerFactory.getLogger(UcFindEntertainmentImpl.class);

  @Override
  public EntertainmentCto findEntertainmentCto(long id) {

    LOG.debug("Get EntertainmentCto with id {} from database.", id);
    EntertainmentEntity entity = getEntertainmentRepository().find(id);
    EntertainmentCto cto = new EntertainmentCto();
    cto.setEntertainment(getBeanMapper().map(entity, EntertainmentEto.class));

    return cto;
  }

  @Override
  public Page<EntertainmentCto> findEntertainmentCtos(EntertainmentSearchCriteriaTo criteria) {

    Page<EntertainmentEntity> entertainments = getEntertainmentRepository().findByCriteria(criteria);
    List<EntertainmentCto> ctos = new ArrayList<>();
    for (EntertainmentEntity entity : entertainments.getContent()) {
      EntertainmentCto cto = new EntertainmentCto();
      cto.setEntertainment(getBeanMapper().map(entity, EntertainmentEto.class));
      ctos.add(cto);
    }
    Pageable pagResultTo = PageRequest.of(criteria.getPageable().getPageNumber(), criteria.getPageable().getPageSize());

    return new PageImpl<>(ctos, pagResultTo, entertainments.getTotalElements());
  }
}
