package com.cap.simulatorservices.entertainmentmanagement.dataaccess.api.repo;

import static com.querydsl.core.alias.Alias.$;

import java.sql.Timestamp;
import java.util.Iterator;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;

import com.cap.simulatorservices.entertainmentmanagement.dataaccess.api.ShowDetailsEntity;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.ShowDetailsSearchCriteriaTo;
import com.devonfw.module.jpa.dataaccess.api.QueryUtil;
import com.devonfw.module.jpa.dataaccess.api.data.DefaultRepository;
import com.querydsl.jpa.impl.JPAQuery;

/**
 * {@link DefaultRepository} for {@link ShowDetailsEntity}
 */
public interface ShowDetailsRepository extends DefaultRepository<ShowDetailsEntity> {

  /**
   * @param criteria the {@link ShowDetailsSearchCriteriaTo} with the criteria to search.
   * @param pageRequest {@link Pageable} implementation used to set page properties like page size
   * @return the {@link Page} of the {@link ShowDetailsEntity} objects that matched the search.
   */
  default Page<ShowDetailsEntity> findByCriteria(ShowDetailsSearchCriteriaTo criteria) {

    ShowDetailsEntity alias = newDslAlias();
    JPAQuery<ShowDetailsEntity> query = newDslQuery(alias);

    Timestamp showTime = criteria.getShowTime();
    if (showTime != null) {
      query.where($(alias.getShowTime()).eq(showTime));
    }
    String classType = criteria.getClassType();
    if (classType != null && !classType.isEmpty()) {
      QueryUtil.get().whereString(query, $(alias.getClassType()), classType, criteria.getClassTypeOption());
    }
    Long entertainmentId = criteria.getEntertainmentIdId();
    if (entertainmentId != null) {
      query.where($(alias.getEntertainment().getId()).eq(entertainmentId));
    }
    addOrderBy(query, alias, criteria.getPageable().getSort());

    return QueryUtil.get().findPaginated(criteria.getPageable(), query, true);
  }

  /**
   * Add sorting to the given query on the given alias
   *
   * @param query to add sorting to
   * @param alias to retrieve columns from for sorting
   * @param sort specification of sorting
   */
  public default void addOrderBy(JPAQuery<ShowDetailsEntity> query, ShowDetailsEntity alias, Sort sort) {

    if (sort != null && sort.isSorted()) {
      Iterator<Order> it = sort.iterator();
      while (it.hasNext()) {
        Order next = it.next();
        switch (next.getProperty()) {
          case "showTime":
            if (next.isAscending()) {
              query.orderBy($(alias.getShowTime()).asc());
            } else {
              query.orderBy($(alias.getShowTime()).desc());
            }
            break;
          case "classType":
            if (next.isAscending()) {
              query.orderBy($(alias.getClassType()).asc());
            } else {
              query.orderBy($(alias.getClassType()).desc());
            }
            break;
          case "entertainmentId":
            if (next.isAscending()) {
              query.orderBy($(alias.getEntertainment().getId()).asc());
            } else {
              query.orderBy($(alias.getEntertainment().getId()).desc());
            }
            break;
          default:
            throw new IllegalArgumentException("Sorted by the unknown property '" + next.getProperty() + "'");
        }
      }
    }
  }

}