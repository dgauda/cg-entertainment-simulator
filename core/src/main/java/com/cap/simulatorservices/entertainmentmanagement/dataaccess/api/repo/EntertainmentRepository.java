package com.cap.simulatorservices.entertainmentmanagement.dataaccess.api.repo;

import static com.querydsl.core.alias.Alias.$;

import java.util.Iterator;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;

import com.cap.simulatorservices.entertainmentmanagement.dataaccess.api.EntertainmentEntity;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.EntertainmentSearchCriteriaTo;
import com.devonfw.module.jpa.dataaccess.api.QueryUtil;
import com.devonfw.module.jpa.dataaccess.api.data.DefaultRepository;
import com.querydsl.jpa.impl.JPAQuery;

/**
 * {@link DefaultRepository} for {@link EntertainmentEntity}
 */
public interface EntertainmentRepository extends DefaultRepository<EntertainmentEntity> {

  /**
   * @param criteria the {@link EntertainmentSearchCriteriaTo} with the criteria to search.
   * @param pageRequest {@link Pageable} implementation used to set page properties like page size
   * @return the {@link Page} of the {@link EntertainmentEntity} objects that matched the search.
   */
  default Page<EntertainmentEntity> findByCriteria(EntertainmentSearchCriteriaTo criteria) {

    EntertainmentEntity alias = newDslAlias();
    JPAQuery<EntertainmentEntity> query = newDslQuery(alias);

    String address = criteria.getAddress();
    if (address != null && !address.isEmpty()) {
      QueryUtil.get().whereString(query, $(alias.getAddress()), address, criteria.getAddressOption());
    }
    Integer zip = criteria.getZip();
    if (zip != null) {
      query.where($(alias.getZip()).eq(zip));
    }
    String entertainmentType = criteria.getEntertainmentType();
    if (entertainmentType != null && !entertainmentType.isEmpty()) {
      QueryUtil.get().whereString(query, $(alias.getEntertainmentType()), entertainmentType,
          criteria.getEntertainmentTypeOption());
    }
    addOrderBy(query, alias, criteria.getPageable().getSort());

    return QueryUtil.get().findPaginated(criteria.getPageable(), query, true);
  }

  /**
   * Add sorting to the given query on the given alias
   * 
   * @param query to add sorting to
   * @param alias to retrieve columns from for sorting
   * @param sort specification of sorting
   */
  public default void addOrderBy(JPAQuery<EntertainmentEntity> query, EntertainmentEntity alias, Sort sort) {

    if (sort != null && sort.isSorted()) {
      Iterator<Order> it = sort.iterator();
      while (it.hasNext()) {
        Order next = it.next();
        switch (next.getProperty()) {
          case "address":
            if (next.isAscending()) {
              query.orderBy($(alias.getAddress()).asc());
            } else {
              query.orderBy($(alias.getAddress()).desc());
            }
            break;
          case "zip":
            if (next.isAscending()) {
              query.orderBy($(alias.getZip()).asc());
            } else {
              query.orderBy($(alias.getZip()).desc());
            }
            break;
          case "entertainmentType":
            if (next.isAscending()) {
              query.orderBy($(alias.getEntertainmentType()).asc());
            } else {
              query.orderBy($(alias.getEntertainmentType()).desc());
            }
            break;
          default:
            throw new IllegalArgumentException("Sorted by the unknown property '" + next.getProperty() + "'");
        }
      }
    }
  }

}