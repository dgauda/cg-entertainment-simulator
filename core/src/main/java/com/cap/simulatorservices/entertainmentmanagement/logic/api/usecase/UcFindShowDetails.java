package com.cap.simulatorservices.entertainmentmanagement.logic.api.usecase;

import java.util.List;

import org.springframework.data.domain.Page;

import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.ShowDetailsCto;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.ShowDetailsSearchCriteriaTo;

public interface UcFindShowDetails {

  /**
   * Returns a composite ShowDetails by its id 'id'.
   *
   * @param id The id 'id' of the ShowDetails.
   * @return The {@link ShowDetailsCto} with id 'id'
   */
  ShowDetailsCto findShowDetailsCto(long id);

  /**
   * Returns a paginated list of composite ShowDetailss matching the search criteria.
   *
   * @param criteria the {@link ShowDetailsSearchCriteriaTo}.
   * @return the {@link List} of matching {@link ShowDetailsCto}s.
   */
  Page<ShowDetailsCto> findShowDetailsCtos(ShowDetailsSearchCriteriaTo criteria);

}
