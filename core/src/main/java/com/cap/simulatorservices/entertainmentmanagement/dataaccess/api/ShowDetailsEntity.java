package com.cap.simulatorservices.entertainmentmanagement.dataaccess.api;

import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.cap.simulatorservices.entertainmentmanagement.common.api.ShowDetails;
import com.cap.simulatorservices.general.dataaccess.api.ApplicationPersistenceEntity;

/**
 * @author lebhatia
 */
@Entity
@Table(name = "Entertainment_Showdetails")
public class ShowDetailsEntity extends ApplicationPersistenceEntity implements ShowDetails {

  private static final long serialVersionUID = 1L;

  private Timestamp showTime;

  private String classType;

  private EntertainmentEntity entertainment;

  public ShowDetailsEntity(Timestamp showTime, String classType, EntertainmentEntity entertainment) {

    super();
    this.showTime = showTime;
    this.classType = classType;
    this.entertainment = entertainment;
  }

  /**
   * The constructor.
   */
  public ShowDetailsEntity() {

    super();
  }

  /**
   * @return the showTime
   */
  public Timestamp getShowTime() {

    return this.showTime;
  }

  /**
   * @param showTime the showTime to set
   */
  public void setShowTime(Timestamp showTime) {

    this.showTime = showTime;
  }

  /**
   * @return classType
   */
  public String getClassType() {

    return this.classType;
  }

  /**
   * @param classType new value of {@link #getclassType}.
   */
  public void setClassType(String classType) {

    this.classType = classType;
  }

  /**
   * @return entertainmentId
   */
  @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
  @JoinColumn(name = "entertainmentid")
  public EntertainmentEntity getEntertainment() {

    return this.entertainment;
  }

  /**
   * @param entertainmentId new value of {@link #getentertainmentId}.
   */
  public void setEntertainment(EntertainmentEntity entertainment) {

    this.entertainment = entertainment;
  }

  @Override
  @Transient
  public Long getEntertainmentId() {

    if (this.entertainment == null) {
      return null;
    }
    return this.entertainment.getId();
  }

  @Override
  public void setEntertainmentId(Long showId) {

    if (showId == null) {
      this.entertainment = null;
    } else {
      EntertainmentEntity entertainmentEntity = new EntertainmentEntity();
      entertainmentEntity.setId(showId);
      this.entertainment = entertainmentEntity;
    }
  }

}
