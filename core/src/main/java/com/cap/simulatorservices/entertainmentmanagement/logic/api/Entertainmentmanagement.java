package com.cap.simulatorservices.entertainmentmanagement.logic.api;

import com.cap.simulatorservices.entertainmentmanagement.logic.api.usecase.UcFindCustomer;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.usecase.UcFindEntertainment;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.usecase.UcFindEntertainmentReservation;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.usecase.UcFindShowDetails;

/**
 * Interface for Entertainmentmanagement component.
 */
public interface Entertainmentmanagement
    extends UcFindCustomer, UcFindEntertainment, UcFindEntertainmentReservation, UcFindShowDetails {

}
