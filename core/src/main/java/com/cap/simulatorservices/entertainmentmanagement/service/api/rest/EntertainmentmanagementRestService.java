package com.cap.simulatorservices.entertainmentmanagement.service.api.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.data.domain.Page;

import com.cap.simulatorservices.entertainmentmanagement.logic.api.Entertainmentmanagement;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.CustomerCto;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.CustomerSearchCriteriaTo;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.EntertainmentCto;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.EntertainmentReservationCto;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.EntertainmentReservationSearchCriteriaTo;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.EntertainmentSearchCriteriaTo;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.ShowDetailsCto;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.ShowDetailsSearchCriteriaTo;

/**
 * The service interface for REST calls in order to execute the logic of component {@link Entertainmentmanagement}.
 */
@Path("/entertainmentmanagement/v1")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public interface EntertainmentmanagementRestService {

  /**
   * Delegates to {@link Entertainmentmanagement#findCustomerCto}.
   *
   * @param id the ID of the {@link CustomerCto}
   * @return the {@link CustomerCto}
   */
  @GET
  @Path("/customer/cto/{id}/")
  public CustomerCto getCustomerCto(@PathParam("id") long id);

  /**
   * Delegates to {@link Entertainmentmanagement#findCustomerCtos}.
   *
   * @param searchCriteriaTo the pagination and search criteria to be used for finding customers.
   * @return the {@link Page list} of matching {@link CustomerCto}s.
   */
  @Path("/customer/cto/search")
  @POST
  public Page<CustomerCto> findCustomerCtos(CustomerSearchCriteriaTo searchCriteriaTo);

  /**
   * Delegates to {@link Entertainmentmanagement#findEntertainmentCto}.
   *
   * @param id the ID of the {@link EntertainmentCto}
   * @return the {@link EntertainmentCto}
   */
  @GET
  @Path("/entertainment/cto/{id}/")
  public EntertainmentCto getEntertainmentCto(@PathParam("id") long id);

  /**
   * Delegates to {@link Entertainmentmanagement#findEntertainmentCtos}.
   *
   * @param searchCriteriaTo the pagination and search criteria to be used for finding entertainments.
   * @return the {@link Page list} of matching {@link EntertainmentCto}s.
   */
  @Path("/entertainment/cto/search")
  @POST
  public Page<EntertainmentCto> findEntertainmentCtos(EntertainmentSearchCriteriaTo searchCriteriaTo);

  /**
   * Delegates to {@link Entertainmentmanagement#findEntertainmentReservationCto}.
   *
   * @param id the ID of the {@link EntertainmentReservationCto}
   * @return the {@link EntertainmentReservationCto}
   */
  @GET
  @Path("/entertainmentreservation/cto/{id}/")
  public EntertainmentReservationCto getEntertainmentReservationCto(@PathParam("id") long id);

  /**
   * Delegates to {@link Entertainmentmanagement#findEntertainmentReservationCtos}.
   *
   * @param searchCriteriaTo the pagination and search criteria to be used for finding entertainmentreservations.
   * @return the {@link Page list} of matching {@link EntertainmentReservationCto}s.
   */
  @Path("/entertainmentreservation/cto/search")
  @POST
  public Page<EntertainmentReservationCto> findEntertainmentReservationCtos(
      EntertainmentReservationSearchCriteriaTo searchCriteriaTo);

  /**
   * Delegates to {@link Entertainmentmanagement#findShowDetailsCto}.
   *
   * @param id the ID of the {@link ShowDetailsCto}
   * @return the {@link ShowDetailsCto}
   */
  @GET
  @Path("/showdetails/cto/{id}/")
  public ShowDetailsCto getShowDetailsCto(@PathParam("id") long id);

  /**
   * Delegates to {@link Entertainmentmanagement#findShowDetailsCtos}.
   *
   * @param searchCriteriaTo the pagination and search criteria to be used for finding showdetailss.
   * @return the {@link Page list} of matching {@link ShowDetailsCto}s.
   */
  @Path("/showdetails/cto/search")
  @POST
  public Page<ShowDetailsCto> findShowDetailsCtos(ShowDetailsSearchCriteriaTo searchCriteriaTo);

}
