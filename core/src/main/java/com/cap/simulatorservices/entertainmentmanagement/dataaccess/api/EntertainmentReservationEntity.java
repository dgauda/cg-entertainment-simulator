package com.cap.simulatorservices.entertainmentmanagement.dataaccess.api;

import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import com.cap.simulatorservices.entertainmentmanagement.common.api.EntertainmentReservation;
import com.cap.simulatorservices.general.dataaccess.api.ApplicationPersistenceEntity;

/**
 * @author lebhatia
 */
@Entity
@Table(name = "Entertainment_Reservation")
public class EntertainmentReservationEntity extends ApplicationPersistenceEntity implements EntertainmentReservation {

  private static final long serialVersionUID = 1L;

  private Timestamp bookingDate;

  private Integer noOfTickets;

  private CustomerEntity customer;

  private ShowDetailsEntity showDetails;

  public EntertainmentReservationEntity(Timestamp bookingDate, Integer noOfTickets, @NotNull CustomerEntity customer,
      @NotNull ShowDetailsEntity showDetails) {

    super();
    this.bookingDate = bookingDate;
    this.noOfTickets = noOfTickets;
    this.customer = this.customer;
    this.showDetails = showDetails;
  }

  /**
   * @return bookingDate
   */
  public Timestamp getBookingDate() {

    return this.bookingDate;
  }

  /**
   * @param bookingDate new value of {@link #getbookingDate}.
   */
  public void setBookingDate(Timestamp bookingDate) {

    this.bookingDate = bookingDate;
  }

  /**
   * @return noOfTickets
   */
  public Integer getNoOfTickets() {

    return this.noOfTickets;
  }

  /**
   * @param noOfTickets new value of {@link #getnoOfTickets}.
   */
  public void setNoOfTickets(Integer noOfTickets) {

    this.noOfTickets = noOfTickets;
  }

  /**
   * @return customerId
   */
  @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.DETACH)
  @JoinColumn(name = "customerid")
  public CustomerEntity getCustomer() {

    return this.customer;
  }

  /**
   * @param customerId new value of {@link #getcustomerId}.
   */
  public void setCustomer(CustomerEntity customer) {

    this.customer = customer;
  }

  /**
   * @return the showId
   */
  @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
  @JoinColumn(name = "showid")
  public ShowDetailsEntity getShowDetails() {

    return this.showDetails;
  }

  /**
   * @param showDetails new value of {@link #getshowDetails}.
   */
  public void setShowDetails(ShowDetailsEntity showDetails) {

    this.showDetails = showDetails;
  }

  /**
   * @param showId the showId to set
   */
  public void setShowId(ShowDetailsEntity showDetails) {

    this.showDetails = showDetails;
  }

  @Override
  @Transient
  public Long getCustomerId() {

    if (this.customer == null) {
      return null;
    }
    return this.customer.getId();
  }

  @Override
  public void setCustomerId(Long customerId) {

    if (customerId == null) {
      this.customer = null;
    } else {
      CustomerEntity customerEntity = new CustomerEntity();
      customerEntity.setId(customerId);
      this.customer = customerEntity;
    }
  }

  @Override
  @Transient
  public Long getShowId() {

    if (this.showDetails == null) {
      return null;
    }
    return this.showDetails.getId();
  }

  @Override
  public void setShowId(Long showId) {

    if (showId == null) {
      this.showDetails = null;
    } else {
      ShowDetailsEntity showDetailsEntity = new ShowDetailsEntity();
      showDetailsEntity.setId(showId);
      this.showDetails = showDetailsEntity;
    }
  }

}
