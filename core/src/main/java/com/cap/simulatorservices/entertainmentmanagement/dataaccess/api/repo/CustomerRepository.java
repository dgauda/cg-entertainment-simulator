package com.cap.simulatorservices.entertainmentmanagement.dataaccess.api.repo;

import static com.querydsl.core.alias.Alias.$;

import java.sql.Timestamp;
import java.util.Iterator;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;

import com.cap.simulatorservices.entertainmentmanagement.dataaccess.api.CustomerEntity;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.CustomerSearchCriteriaTo;
import com.devonfw.module.jpa.dataaccess.api.QueryUtil;
import com.devonfw.module.jpa.dataaccess.api.data.DefaultRepository;
import com.querydsl.jpa.impl.JPAQuery;

/**
 * {@link DefaultRepository} for {@link CustomerEntity}
 */
public interface CustomerRepository extends DefaultRepository<CustomerEntity> {

  /**
   * @param criteria the {@link CustomerSearchCriteriaTo} with the criteria to search.
   * @param pageRequest {@link Pageable} implementation used to set page properties like page size
   * @return the {@link Page} of the {@link CustomerEntity} objects that matched the search.
   */
  default Page<CustomerEntity> findByCriteria(CustomerSearchCriteriaTo criteria) {

    CustomerEntity alias = newDslAlias();
    JPAQuery<CustomerEntity> query = newDslQuery(alias);

    String name = criteria.getName();
    if (name != null && !name.isEmpty()) {
      QueryUtil.get().whereString(query, $(alias.getName()), name, criteria.getNameOption());
    }
    Integer contactNumber = criteria.getContactNumber();
    if (contactNumber != null) {
      query.where($(alias.getContactNumber()).eq(contactNumber));
    }
    Boolean isRegularCustomer = criteria.getIsRegularCustomer();
    if (isRegularCustomer != null) {
      query.where($(alias.getIsRegularCustomer()).eq(isRegularCustomer));
    }
    Timestamp registrationDate = criteria.getRegistrationDate();
    if (registrationDate != null) {
      query.where($(alias.getRegistrationDate()).eq(registrationDate));
    }
    addOrderBy(query, alias, criteria.getPageable().getSort());

    return QueryUtil.get().findPaginated(criteria.getPageable(), query, true);
  }

  /**
   * Add sorting to the given query on the given alias
   *
   * @param query to add sorting to
   * @param alias to retrieve columns from for sorting
   * @param sort specification of sorting
   */
  public default void addOrderBy(JPAQuery<CustomerEntity> query, CustomerEntity alias, Sort sort) {

    if (sort != null && sort.isSorted()) {
      Iterator<Order> it = sort.iterator();
      while (it.hasNext()) {
        Order next = it.next();
        switch (next.getProperty()) {
          case "name":
            if (next.isAscending()) {
              query.orderBy($(alias.getName()).asc());
            } else {
              query.orderBy($(alias.getName()).desc());
            }
            break;
          case "contactNumber":
            if (next.isAscending()) {
              query.orderBy($(alias.getContactNumber()).asc());
            } else {
              query.orderBy($(alias.getContactNumber()).desc());
            }
            break;
          case "isRegularCustomer":
            if (next.isAscending()) {
              query.orderBy($(alias.getIsRegularCustomer()).asc());
            } else {
              query.orderBy($(alias.getIsRegularCustomer()).desc());
            }
            break;
          case "registrationDate":
            if (next.isAscending()) {
              query.orderBy($(alias.getRegistrationDate()).asc());
            } else {
              query.orderBy($(alias.getRegistrationDate()).desc());
            }
            break;
          default:
            throw new IllegalArgumentException("Sorted by the unknown property '" + next.getProperty() + "'");
        }
      }
    }
  }

}