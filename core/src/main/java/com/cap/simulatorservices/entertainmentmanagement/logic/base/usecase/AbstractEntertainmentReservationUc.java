package com.cap.simulatorservices.entertainmentmanagement.logic.base.usecase;

import javax.inject.Inject;

import com.cap.simulatorservices.entertainmentmanagement.dataaccess.api.repo.EntertainmentReservationRepository;
import com.cap.simulatorservices.general.logic.base.AbstractUc;

/**
 * Abstract use case for EntertainmentReservations, which provides access to the commonly necessary data access objects.
 */
public class AbstractEntertainmentReservationUc extends AbstractUc {

  /** @see #getEntertainmentReservationRepository() */
  @Inject
  private EntertainmentReservationRepository entertainmentReservationRepository;

  /**
   * Returns the field 'entertainmentReservationRepository'.
   * 
   * @return the {@link EntertainmentReservationRepository} instance.
   */
  public EntertainmentReservationRepository getEntertainmentReservationRepository() {

    return this.entertainmentReservationRepository;
  }

}
