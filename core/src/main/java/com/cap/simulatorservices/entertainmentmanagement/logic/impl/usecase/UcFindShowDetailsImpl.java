package com.cap.simulatorservices.entertainmentmanagement.logic.impl.usecase;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.cap.simulatorservices.entertainmentmanagement.dataaccess.api.ShowDetailsEntity;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.EntertainmentEto;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.ShowDetailsCto;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.ShowDetailsEto;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.ShowDetailsSearchCriteriaTo;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.usecase.UcFindShowDetails;
import com.cap.simulatorservices.entertainmentmanagement.logic.base.usecase.AbstractShowDetailsUc;

/**
 * Use case implementation for searching, filtering and getting ShowDetailss
 */
@Named
@Validated
@Transactional
public class UcFindShowDetailsImpl extends AbstractShowDetailsUc implements UcFindShowDetails {

  /** Logger instance. */
  private static final Logger LOG = LoggerFactory.getLogger(UcFindShowDetailsImpl.class);

  @Override
  public ShowDetailsCto findShowDetailsCto(long id) {

    LOG.debug("Get ShowDetailsCto with id {} from database.", id);
    ShowDetailsEntity entity = getShowDetailsRepository().find(id);
    ShowDetailsCto cto = new ShowDetailsCto();
    cto.setShowDetails(getBeanMapper().map(entity, ShowDetailsEto.class));
    cto.setEntertainmentId(getBeanMapper().map(entity.getEntertainmentId(), EntertainmentEto.class));

    return cto;
  }

  @Override
  public Page<ShowDetailsCto> findShowDetailsCtos(ShowDetailsSearchCriteriaTo criteria) {

    Page<ShowDetailsEntity> showdetailss = getShowDetailsRepository().findByCriteria(criteria);
    List<ShowDetailsCto> ctos = new ArrayList<>();
    for (ShowDetailsEntity entity : showdetailss.getContent()) {
      ShowDetailsCto cto = new ShowDetailsCto();
      cto.setShowDetails(getBeanMapper().map(entity, ShowDetailsEto.class));
      cto.setEntertainmentId(getBeanMapper().map(entity.getEntertainmentId(), EntertainmentEto.class));
      ctos.add(cto);
    }
    Pageable pagResultTo = PageRequest.of(criteria.getPageable().getPageNumber(), criteria.getPageable().getPageSize());

    return new PageImpl<>(ctos, pagResultTo, showdetailss.getTotalElements());
  }
}
