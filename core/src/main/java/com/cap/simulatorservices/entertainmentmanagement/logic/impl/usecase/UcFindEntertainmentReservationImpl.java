package com.cap.simulatorservices.entertainmentmanagement.logic.impl.usecase;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.cap.simulatorservices.entertainmentmanagement.dataaccess.api.EntertainmentReservationEntity;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.CustomerEto;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.EntertainmentReservationCto;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.EntertainmentReservationEto;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.EntertainmentReservationSearchCriteriaTo;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.ShowDetailsEto;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.usecase.UcFindEntertainmentReservation;
import com.cap.simulatorservices.entertainmentmanagement.logic.base.usecase.AbstractEntertainmentReservationUc;

/**
 * Use case implementation for searching, filtering and getting EntertainmentReservations
 */
@Named
@Validated
@Transactional
public class UcFindEntertainmentReservationImpl extends AbstractEntertainmentReservationUc
    implements UcFindEntertainmentReservation {

  /** Logger instance. */
  private static final Logger LOG = LoggerFactory.getLogger(UcFindEntertainmentReservationImpl.class);

  @Override
  public EntertainmentReservationCto findEntertainmentReservationCto(long id) {

    LOG.debug("Get EntertainmentReservationCto with id {} from database.", id);
    EntertainmentReservationEntity entity = getEntertainmentReservationRepository().find(id);
    EntertainmentReservationCto cto = new EntertainmentReservationCto();
    cto.setEntertainmentReservation(getBeanMapper().map(entity, EntertainmentReservationEto.class));
    cto.setCustomerId(getBeanMapper().map(entity.getCustomerId(), CustomerEto.class));
    cto.setShowId(getBeanMapper().map(entity.getShowId(), ShowDetailsEto.class));

    return cto;
  }

  @Override
  public Page<EntertainmentReservationCto> findEntertainmentReservationCtos(
      EntertainmentReservationSearchCriteriaTo criteria) {

    Page<EntertainmentReservationEntity> entertainmentreservations = getEntertainmentReservationRepository()
        .findByCriteria(criteria);
    List<EntertainmentReservationCto> ctos = new ArrayList<>();
    for (EntertainmentReservationEntity entity : entertainmentreservations.getContent()) {
      EntertainmentReservationCto cto = new EntertainmentReservationCto();
      cto.setEntertainmentReservation(getBeanMapper().map(entity, EntertainmentReservationEto.class));
      cto.setCustomerId(getBeanMapper().map(entity.getCustomerId(), CustomerEto.class));
      cto.setShowId(getBeanMapper().map(entity.getShowId(), ShowDetailsEto.class));
      ctos.add(cto);
    }
    Pageable pagResultTo = PageRequest.of(criteria.getPageable().getPageNumber(), criteria.getPageable().getPageSize());

    return new PageImpl<>(ctos, pagResultTo, entertainmentreservations.getTotalElements());
  }
}
