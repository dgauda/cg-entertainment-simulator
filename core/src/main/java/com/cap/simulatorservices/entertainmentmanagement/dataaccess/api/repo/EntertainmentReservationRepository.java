package com.cap.simulatorservices.entertainmentmanagement.dataaccess.api.repo;

import static com.querydsl.core.alias.Alias.$;

import java.sql.Timestamp;
import java.util.Iterator;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;

import com.cap.simulatorservices.entertainmentmanagement.dataaccess.api.EntertainmentReservationEntity;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.EntertainmentReservationSearchCriteriaTo;
import com.devonfw.module.jpa.dataaccess.api.QueryUtil;
import com.devonfw.module.jpa.dataaccess.api.data.DefaultRepository;
import com.querydsl.jpa.impl.JPAQuery;

/**
 * {@link DefaultRepository} for {@link EntertainmentReservationEntity}
 */
public interface EntertainmentReservationRepository extends DefaultRepository<EntertainmentReservationEntity> {

  /**
   * @param criteria the {@link EntertainmentReservationSearchCriteriaTo} with the criteria to search.
   * @param pageRequest {@link Pageable} implementation used to set page properties like page size
   * @return the {@link Page} of the {@link EntertainmentReservationEntity} objects that matched the search.
   */
  default Page<EntertainmentReservationEntity> findByCriteria(EntertainmentReservationSearchCriteriaTo criteria) {

    EntertainmentReservationEntity alias = newDslAlias();
    JPAQuery<EntertainmentReservationEntity> query = newDslQuery(alias);

    Timestamp bookingDate = criteria.getBookingDate();
    if (bookingDate != null) {
      query.where($(alias.getBookingDate()).eq(bookingDate));
    }
    Integer noOfTickets = criteria.getNoOfTickets();
    if (noOfTickets != null) {
      query.where($(alias.getNoOfTickets()).eq(noOfTickets));
    }
    Long customerId = criteria.getCustomerIdId();
    if (customerId != null) {
      query.where($(alias.getCustomerId()).eq(customerId));
    }
    Long showId = criteria.getShowIdId();
    if (showId != null) {
      query.where($(alias.getShowDetails().getId()).eq(showId));
    }
    addOrderBy(query, alias, criteria.getPageable().getSort());

    return QueryUtil.get().findPaginated(criteria.getPageable(), query, true);
  }

  /**
   * Add sorting to the given query on the given alias
   *
   * @param query to add sorting to
   * @param alias to retrieve columns from for sorting
   * @param sort specification of sorting
   */
  public default void addOrderBy(JPAQuery<EntertainmentReservationEntity> query, EntertainmentReservationEntity alias,
      Sort sort) {

    if (sort != null && sort.isSorted()) {
      Iterator<Order> it = sort.iterator();
      while (it.hasNext()) {
        Order next = it.next();
        switch (next.getProperty()) {
          case "bookingDate":
            if (next.isAscending()) {
              query.orderBy($(alias.getBookingDate()).asc());
            } else {
              query.orderBy($(alias.getBookingDate()).desc());
            }
            break;
          case "noOfTickets":
            if (next.isAscending()) {
              query.orderBy($(alias.getNoOfTickets()).asc());
            } else {
              query.orderBy($(alias.getNoOfTickets()).desc());
            }
            break;
          case "customerId":
            if (next.isAscending()) {
              query.orderBy($(alias.getCustomerId()).asc());
            } else {
              query.orderBy($(alias.getCustomerId()).desc());
            }
            break;
          case "showId":
            if (next.isAscending()) {
              query.orderBy($(alias.getShowDetails().getId()).asc());
            } else {
              query.orderBy($(alias.getShowDetails().getId()).desc());
            }
            break;
          default:
            throw new IllegalArgumentException("Sorted by the unknown property '" + next.getProperty() + "'");
        }
      }
    }
  }

}