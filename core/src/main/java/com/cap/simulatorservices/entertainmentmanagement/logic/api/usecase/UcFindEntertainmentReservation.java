package com.cap.simulatorservices.entertainmentmanagement.logic.api.usecase;

import java.util.List;

import org.springframework.data.domain.Page;

import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.EntertainmentReservationCto;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.EntertainmentReservationSearchCriteriaTo;

public interface UcFindEntertainmentReservation {

  /**
   * Returns a composite EntertainmentReservation by its id 'id'.
   *
   * @param id The id 'id' of the EntertainmentReservation.
   * @return The {@link EntertainmentReservationCto} with id 'id'
   */
  EntertainmentReservationCto findEntertainmentReservationCto(long id);

  /**
   * Returns a paginated list of composite EntertainmentReservations matching the search criteria.
   *
   * @param criteria the {@link EntertainmentReservationSearchCriteriaTo}.
   * @return the {@link List} of matching {@link EntertainmentReservationCto}s.
   */
  Page<EntertainmentReservationCto> findEntertainmentReservationCtos(EntertainmentReservationSearchCriteriaTo criteria);

}
